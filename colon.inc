%define next 0		
%macro colon 2	


    %ifid %2
        %2:
            dq next
            %define next %2

    %else:
        %fatal: "id expected"

    %endif

    %ifstr %1
        db %1, 0

    %else
		%fatal "string expected"

    %endif

%endmacro



