%include "words.inc"
%include "lib.inc"

%define buffer_size 256

global _start


section .rodata
    input_key: db 'Enter the key: ', 0
    success: db 'Found element with this key: ', 0
    error: db 'Incorrect key', 0
    overflow: db 'Input string is too long', 0

section .bss
    input_buffer: resb buffer_size

    

section .text

    _start:
        mov rdi, input_key
        call print_string
        mov rdi, input_buffer
        mov rsi, buffer_size
        call read_word
        cmp rax, 0
        je .size_error
        mov rsi, next
        mov rdi, rax
        call find_word
        cmp rax, 0
        je .not_found_error

        .print_key:
            mov rdi,rax
            add rdi, 8
            push rdi
            call string_length
            pop rdi
            add rdi, rax
            inc rdi
            mov rsi,1
            call print_string
            call print_newline
            add rsp, buffer_size
            xor rdi, rdi
            call exit

        .size_error:
            mov rdi, overflow
            call print_err_string
            call exit     

        .not_found_error:
           mov rdi, error
           call print_err_string
           call exit  

          

       

