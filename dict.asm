section .text
global find_word

extern string_equals

find_word:
        .iterate:
                cmp rsi, 0
                je .end
                push rdi
                push rsi
                add rsi, 8
                call string_equals
                pop rsi
                pop rdi
                cmp rax, 0
                jne .found
                mov rsi, [rsi]
                cmp rsi, 0		
                jne .iterate

        .found:
            mov rax, rsi
            ret

        .end:
	    xor rax, rax
            ret

